import { useEffect, useRef, useState } from 'react'
import {Howl} from 'howler'
import * as mobilenet from '@tensorflow-models/mobilenet'
import * as knnClassifier from '@tensorflow-models/knn-classifier'
import * as tf from '@tensorflow/tfjs';

import './App.css'

import soundURL from '../src/assets/amthanh.mp3'

var sound = new Howl({
  src:[soundURL]
})
const NOT_TOUCH_LABEL = "not_toach";
const TOUCH_LABEL = 'touched';
const TRAINING_TIMES = 50;
const DEGREE_OF_TRUST = 0.8;
function App() {
  const [touch,setTouch]= useState(false);
  const video = useRef();
  const canPlaySound = useRef(true);
  const classifier = useRef();
  const mobilenetModule = useRef();
  const init = async () => {
    await tf.setBackend('webgl');
    await setUpCamera();
    classifier.current = knnClassifier.create();
    mobilenetModule.current = await mobilenet.load();
  }

  const setUpCamera = () => {
    return new Promise((resolve, reject) => {
      navigator.getUserMedia =
        navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia;
      if (navigator.getUserMedia) {
        navigator.getUserMedia(
          { video: { width: 1280, height: 720 } },
          (stream) => {
            video.current.srcObject = stream;
            video.current.addEventListener('loadeddata', resolve)
          },
          error => reject(error)
        );
      } else {
        reject();
      }
    })
  }
  useEffect(() => {
    init();
     sound.on('end',function(){
        canPlaySound.current=true;
     })
    return () => {

    }
  }, [])
  
  const run = async ()=>{
    const embedding = mobilenetModule.current.infer(
      video.current,
      true
    );
    const result = await classifier.current.predictClass(embedding);
    
    if(result.label == TOUCH_LABEL && result.confidences[result.label]>DEGREE_OF_TRUST){
      
      setTouch(true)
      if(canPlaySound.current){
        console.log("Touch");
        canPlaySound.current = false;
        sound.play();
      }
    }else{
      setTouch(false)
    }
    await sleep(200);
    run();
  }
  
  const train = async label => {
    for (let i = 0; i < TRAINING_TIMES; i++) {
      console.log(`Progress ${parseInt((i+1)/TRAINING_TIMES * 100)}%`)
      await training(label);
    }
   
  }
  const training = label => {
    return new Promise(async resolve =>{
      const embedding = mobilenetModule.current.infer(
        video.current,
        true
      );
      classifier.current.addExample(embedding,label);
      await sleep(100);
      resolve();
    })
  }
  const sleep = (ms=0)=>{
    return new Promise(resolve => setTimeout(resolve,ms))
  }

  return (
    <>
      <div className='main'>
        <video
          ref={video}
          className='video'
          autoPlay
          style={{ transform: 'scaleX(-1)' }}
        ></video >
        <div className="control">
          <button className='btn' onClick={() => train(NOT_TOUCH_LABEL)}>Train1</button>
          <button className='btn' onClick={() => train(TOUCH_LABEL)}>Train2</button>
          <button className='btn' onClick={() => run()}>Run</button>
        </div>
      </div>
    </>
  )
}

export default App
